﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorBright {
    private Dictionary<ColorGroup, float> brightByGroup = new Dictionary<ColorGroup, float>();

    public ChangeColorBright() {
        foreach (ColorGroup group in (ColorGroup[]) Enum.GetValues(typeof(ColorGroup))) {
            brightByGroup.Add(group, 0);
        }
    }
    
    public void ApplyBrightValues(AmplitudeDetection amplitudeDetection) {
        foreach (ColorGroup group in (ColorGroup[]) Enum.GetValues(typeof(ColorGroup))) {
            brightByGroup[group] = amplitudeDetection.maxAmplitudeForBrights[group];
        }
    }

    public Color32[] ApplyNewColors(HSBColor[] hsbColors, Dictionary<ColorGroup, List<int>> pixelsIndexByGroup) {
        Color32[] colors32 = new Color32[hsbColors.Length];
        foreach (var pair in pixelsIndexByGroup) {
            var list = pixelsIndexByGroup[pair.Key];
            for (int ListIndex = 0; ListIndex < list.Count; ListIndex++) {
                HSBColor hsbColor = hsbColors[list[ListIndex]];
                hsbColor.b *= brightByGroup[pair.Key];
                colors32[list[ListIndex]] = hsbColor.ToColor();
            }
        }
        return colors32;
    }
}
