
using System;
using System.IO;
using System.Linq;
using Unity.Collections;
#if UNITY_EDITOR
using UnityEditor.Media;
#endif
using UnityEngine;
using UTJ.FrameCapturer;

public class MovieRecorder : MonoBehaviour
{
    [SerializeField] private Hub hub;

    private float currentTime;
    private bool isVideoStarted;
    public int framesPerSecond = 30;
    public int videoDuration = 10;   

    // [SerializeField] protected DataPath m_outputDir = new DataPath(DataPath.Root.Current, "Capture"); //путь к файлу

    string fileLocation;
#if UNITY_EDITOR

    public void BeginRecorder()
    {
        isVideoStarted = true;
        currentTime = 0;
        GetFilePath();
        ProceedVideo();
    }

    public void GetFilePath()
    {
        string m_Path = Application.dataPath;
        fileLocation = m_Path + "/Resources/CaptureVideo";

    }


    private void ProceedVideo()
    {
        // string outPath = "C:\\FlowPicVideo\\";
        // string outPath = m_outputDir.GetFullPath();


        var videoAttr = new VideoTrackAttributes
        {
            frameRate = new MediaRational(framesPerSecond),
            width = (uint)hub.drawPicture.drawTexture.width,
            height = (uint)hub.drawPicture.drawTexture.height,
            includeAlpha = false
        };

        var audioAttr = new AudioTrackAttributes
        {
            sampleRate = new MediaRational(hub.soundManager.audioSource.clip.frequency),
            channelCount = (ushort)hub.soundManager.audioSource.clip.channels,
            language = "fr"
        };

        int sampleFramesPerVideoFrame = audioAttr.channelCount *
            audioAttr.sampleRate.numerator / videoAttr.frameRate.numerator;

        var encodedFilePath = Path.Combine(fileLocation, $"my_movie_{DateTime.Now:ddMMyy__HHmmss}.mp4");

        float dt = 1.0f / videoAttr.frameRate.numerator;

        var samples = hub.soundManager.multiChannelSamples.ToList();

        using (var encoder = new MediaEncoder(encodedFilePath, videoAttr, audioAttr))
        {
            int framesToRender = framesPerSecond * videoDuration;
            for (int i = 0; i < framesToRender; i++)
            {
                currentTime += dt;
                int frameIndex = hub.soundManager.GetIndexFromTime(currentTime);
                hub.soundManager.UpdatingArrays(frameIndex);
                hub.amplitudeDetection.RenderFrame(false);

                var colors = hub.changeColorBright.ApplyNewColors(hub.imageManager.GetHSBColors(), hub.сolorArrays.pixelIndexesByGroup);
                hub.drawPicture.RenderNewPixels(colors);
                encoder.AddFrame(hub.drawPicture.drawTexture);

                int position = i * sampleFramesPerVideoFrame;
                var frameSamples = samples.GetRange(position, sampleFramesPerVideoFrame);

                var audioBuffer = new NativeArray<float>(sampleFramesPerVideoFrame, Allocator.Temp);
                for (int j = 0; j < sampleFramesPerVideoFrame; j++)
                {
                    audioBuffer[j] = frameSamples[j];
                }

                encoder.AddSamples(audioBuffer);

            }
        }
    }

    public void EndRecording()
    {
        isVideoStarted = false;
    }
#endif
}