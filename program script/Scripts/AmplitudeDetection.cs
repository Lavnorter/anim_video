﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AmplitudeDetection : MonoBehaviour
{
    [Serializable]
    public class AmplitudeData
    {
        public ColorGroup colorGroup;
        public int pointMax;
    }

    [SerializeField] private List<AmplitudeData> m_datas = default;
    public Dictionary<ColorGroup, int> pointMaxByGroup = new Dictionary<ColorGroup, int>();

    public Dictionary<ColorGroup, float> maxAmplitudeForBrights = new Dictionary<ColorGroup, float>();

    private SoundManager soundManager;
    private ChangeColorBright changeColorBright;

    [SerializeField]
    public List<int> pointMaxValues = new List<int>();
    public bool manulaRender;

    public void Setup(SoundManager soundManager, ChangeColorBright changeColorBright)
    {
        this.soundManager = soundManager;
        this.changeColorBright = changeColorBright;

        foreach (AmplitudeData amplitudeData in m_datas)
        {
            pointMaxByGroup.Add(amplitudeData.colorGroup, amplitudeData.pointMax);
            pointMaxValues.Add(amplitudeData.pointMax);
        }

       // ColorSettings();
    }
    /*
    public void ColorSettings()
    {
        foreach (AmplitudeData amplitudeData in m_datas)
        {
            pointMaxByGroup.Remove(amplitudeData.colorGroup);
            pointMaxByGroup.Add(amplitudeData.colorGroup, pointMaxValues[(int)amplitudeData.colorGroup]);
        }
    }
   */

    public void RenderFrame(bool realtimeRender)
    {
        foreach (KeyValuePair<ColorGroup, int> pair in pointMaxByGroup)
        {
            /*
            maxAmplitudeForBrights[pair.Key] =
                   Mathf.Clamp(soundManager.soundsByGroup[pair.Key].Max() * pointMaxByGroup[pair.Key], 0.5f, 1f);
            */
            
            if (!realtimeRender)
            {
                maxAmplitudeForBrights[pair.Key] =
                    Mathf.Clamp(soundManager.soundsByGroup[pair.Key].Max() * pointMaxByGroup[pair.Key], 0.5f, 1f);
            }
            else
            {
                maxAmplitudeForBrights[pair.Key] = pointMaxByGroup[pair.Key];
            }
            

        }

        changeColorBright.ApplyBrightValues(this);
    }


}
