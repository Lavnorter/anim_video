﻿using UnityEngine;

public class ImageManager
{
    private HSBColor[] m_originalHBSPix;
    private Color32[] m_originalPix;

    public ImageManager(Color32[] originalPix) {
        m_originalPix = originalPix;
        m_originalHBSPix = ConvertToHBS(m_originalPix);
    }

    private HSBColor[] ConvertToHBS(Color32[] colors32) {
        HSBColor[] shbColors = new HSBColor[colors32.Length];
        for (var i = 0; i < colors32.Length; i++) {
            Color32 color32 = colors32[i];
            shbColors[i] = new HSBColor(color32);
        }
        return shbColors;
    }

    public HSBColor[] GetHSBColors() {
        return m_originalHBSPix;
    }
    public Color32[] GetColors32() {
        return m_originalPix;
    }
}

