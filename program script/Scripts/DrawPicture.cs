﻿using UnityEngine;

public class DrawPicture {
    public Texture2D drawTexture;
    private Color32[] m_originalPix;

    public DrawPicture(Texture2D texture2D)
    {
        drawTexture = new Texture2D(texture2D.width, texture2D.height, TextureFormat.RGBA32, false);
    }

    public void RenderNewPixels(Color32[] new_colors)
    {
        drawTexture.SetPixels32(new_colors);
        drawTexture.Apply(); 
    }
}